#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# This script automates creation of snapshots under gitlab-production project
set -eufo pipefail
IFS=$'\t\n'

### Command requirements
command -v gcloud >/dev/null 2>/dev/null || { echo 'Please install gcloud utility'; exit 1; }

GCE_ZONE=${GCE_ZONE:-us-east1-c}
GCE_PROJECT=${GCE_PROJECT:-gitlab-production}
# disks to snapshot selector
LABEL='do_snapshots=true'

CI_JOB_ID=${CI_JOB_ID:-notonCI}

# description
DESCRIPTION="snapshot created by CI job '${CI_JOB_ID}' on $(date)"

# snapshots before this date will be deleted
# TODO: change to 14 days, this is set for testing
CLEANUP_DATE="$(date --date="1 day ago" +%Y-%m-%d)"

# Set zone in config if not set
gcloud config get-value compute/zone >/dev/null || \
	gcloud config set compute/zone "${GCE_ZONE}"

echo "Querying disks to snapshot (labels.${LABEL}) ... "
declare -a TO_SNAPSHOT
# TODO: check page size
readarray -t TO_SNAPSHOT < <(gcloud compute disks \
	list \
	--filter="labels.${LABEL}" \
	--format="value(name)")

# NOTE: async is not only good, it also removes the need for compute.zoneOperations.get permission :)
echo "Creating snapshots ..."
gcloud compute disks snapshot "${TO_SNAPSHOT[@]}" \
	--description="${DESCRIPTION}" \
	--async \
	--quiet

echo -n "Querying snapshots to delete (older than ${CLEANUP_DATE}) ... "
declare -a TO_DELETE
readarray -t TO_DELETE < <(gcloud compute snapshots list \
	--filter="creationTimestamp<'${CLEANUP_DATE}'" \
	--format="value(name)")
echo "${#TO_DELETE[@]} snapshots found."

if [[ ${#TO_DELETE[@]} -gt 0 ]]; then
	echo "Cleaning up ${#TO_DELETE[@]} old snapshots ..."
	gcloud compute snapshots delete "${TO_DELETE[@]}" \
		--quiet
fi
