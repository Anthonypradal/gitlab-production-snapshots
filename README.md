[![pipeline status](https://gitlab.com/gitlab-restore/gitlab-production-snapshots/badges/master/pipeline.svg)](https://gitlab.com/gitlab-restore/gitlab-production-snapshots/commits/master)

 
### How to enable snapshots for a drive
 - add `do_snapshots=true` label to the drive:
```
gcloud compute disks add-labels drive-name-that-you-want-to-be-snapshotted --labels=do_snapshots=true
```
Next time CI job will run it'll include those disks.

### Service account creation notes
In order to limit the potential security risks, the following were done in `gitlab-production` project:
  - go to IAM & admin -> Service accounts
  - create new role called `CustomRoleGitLabCI_Snapshotter` with with following permissions:
    - `compute.disks.list`
    - `compute.snapshots.create`
    - `compute.disks.createSnapshot`
    - `compute.snapshots.list`  NOTE: these two are for snapshot [cleanup](https://gitlab.com/gitlab-restore/gitlab-production-snapshots/blob/master/snapshot.sh#L43-53)
    - `compute.snapshots.delete` It is a security risk to do that from CI, but at this moment I'm not sure how to mitigate it best, via moving this functionality
      to another project, or periodically copying snapshots themselves somewhere.
  - create new service account named `gitlab-ci-snapshot-bot` as member of this role, check 'furnish json key'
  - the json key should be base64 encoded and added as `GCE_KEY` variable below

### Project creation notes
(TODO: automate the template across all gitlab-restore projects)

 - create `gitlab-production-snapshots` project, make sure its public
 - go to [General Settings](https://gitlab.com/gitlab-restore/gitlab-production-snapshots/edit) and:
   - under Permissions:
     - disable issues
     - disable container registry
     - disable wikis
     - disable snippets
   - under MR settings:
     - enforce pipelines success
     - enforce discussion resolution
 - go to [Repository Settings](https://gitlab.com/gitlab-restore/gitlab-production-snapshots/settings/repository) and:
   - under Push rules enable all restrictions
   - under Protected branches protect master (assuming there's initial commit done)
 - go to [CI/CD Settings](https://gitlab.com/gitlab-restore/gitlab-production-snapshots/settings/ci_cd) and:
   - under General:
      - disable autodevops
      - uncheck public pipelines
      - uncheck pipeline cancelling
   - under Secret Variables:
      - add `GCE_KEY` secret variable
